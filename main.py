import pyshark
import pyfiglet
banner = pyfiglet.figlet_format("Find IT", font = 'nancyj-underlined')
print(banner)
print()
print('Search for MAC Address OUI by vendor on local network')
print('You can look up the MAC prefix @ https://www.wireshark.org/tools/oui-lookup.html')
vendor = input('Enter Mac:')

capture = pyshark.LiveCapture(display_filter='eth.addr[0:3]==' + str(vendor))

for packet in capture.sniff_continuously():
   try:

       #print(packet[0].src)
       print(packet[1].src)
       #print(packet[2].src)
       #print(packet[3].src)
       print(packet[1].dst)

   except:
       pass
